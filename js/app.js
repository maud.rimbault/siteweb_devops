const images = document.querySelectorAll(".hero__img img");
const prev = document.getElementById("sliderPrev");
const next = document.getElementById("sliderNext");
let imageToDisplay = 0;

const bottomButtons = document.querySelectorAll(".slider__bottom");

sliderDisplayer();

function sliderDisplayer(){
    images.forEach((image, index)=>{
        if(index!==imageToDisplay){
            image.style.display = "none";
        } else {
            image.style.display = "block";
        }
    })
}


//console.log(prev);
document.body.addEventListener('click', function(e) {
    //console.log(e.target);
    if(e.target.closest('#sliderPrev')){
        imageToDisplay--;
        if(imageToDisplay<0){
            imageToDisplay = images.length-1;
        }
    } 
    if(e.target.closest('#sliderNext')){
        imageToDisplay++;
        if(imageToDisplay > images.length-1){
            imageToDisplay = 0;
        } 
    }
    if(e.target.closest('.slider__bottom')){
        let x = e.target.closest('.slider__bottom');
        console.log(x.getAttribute('data-index'));
        imageToDisplay = parseInt(x.getAttribute('data-index'));
        console.log(imageToDisplay);
        //console.log(getAttribute('data-index'));
    }
    sliderDisplayer();
}
)




// document.body.addEventListener('click', function(e) {
//     //console.log(e.target);
//     if(e.target.closest('#bottomButtons')){
//         getAttribute(data-index);
//         imageToDisplay--;
//         if(imageToDisplay<0){
//             imageToDisplay = images.length-1;
//         }
//         sliderDisplayer();
//         console.log(imageToDisplay);
//     }
// })

// console.log(bottomButtons);